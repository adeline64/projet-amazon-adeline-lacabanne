# Amazon

## Tech
- HTML
- CSS

## Architecture
- index.html
- readme.md
- .gitignore
    - images
        - mobile-blanc-vignette.jpg
        - mobile-blanc.jpg
        - mobile-noir-vignette-1.jpg
        - mobile-noir-vignette-2.jpg
        - mobile-noir.jpg
        - promo.jpg
    - CSS
        - style.css
    
    ## Consignes
    - Réaliser une page web selon une maquette existante
    - Faire une bonne architecture de projet
    - Réaliser des positionnements sur différents blocs